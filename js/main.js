function showMenu(){
		
	$('.nav-links').css("overflow-y", function(_,val){ 
           return val == "hidden" ? "visible" : "hidden";
      });
}

//create an instance of ApiGetClient to get more users	
function loadMore(){
	
	 var options = {
		headers: {
		  Accept: 'application/json'
		},
		method: 'get',
		queryparams: {"results": 5, "inc":"name,email,picture", "nat":"CA", "noinfo":""},
		apibaseurl: 'https://randomuser.me/',
		apipath: 'api/',
		onComplete: function(response) {
			updatePage(response);
		}
  };
	  
	new ApiGetClient(options);
}

function updatePage(response){
	
	var response_array = JSON.parse(response);
	
	var htmlToAdd = "";
	
	//loop through the response array to get the member information
	for (var item in response_array.results) {
		if (response_array.results.hasOwnProperty(item)) {
			
			var user = response_array.results[item];
			
			var email = user.email.replace("example","igloo");
			var name = user.name.first + " " + user.name.last;
			var pic = user.picture.large;
			
			//add  info from each user
			htmlToAdd += '<div class="member-item"><img class="img-circle" src="'+pic+'"/><p class="member-name">'+name+'</p><p class="member-email"><a href="'+email+'">'+email+'</a></p></div>';
			
		}

		
	}
	
	//encase the html in a row with fade-in CSS
	htmlToAdd = '<div class="member-items-row new-members-fade-in">' + htmlToAdd + '</div>';
	
	//append the new members markup to the last member items row
	$( ".member-items-container").append(htmlToAdd);
	
}